<nav>
    <ul>
        <li id="btn-home" >
            <h3>Home</h3>
            <span id="span-home" class="active"></span>
        </li>
        <li id="btn-about">
            <h3>About</h3>
            <span id="span-about" class="inactive"></span>
        </li>
        <li id="btn-portfolio">
            <h3>Portfolio</h3>
            <span id="span-portfolio" class="inactive"></span>
        </li>
        <li id="btn-contact">
            <h3>Contact</h3>
            <span id="span-contact" class="inactive"></span>
        </li>
    </ul>
</nav>